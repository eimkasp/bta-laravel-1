@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <h1>
                    Savininkai
                </h1>
            </div>
            <div class="col-sm-4">
                @auth
                    @if(Auth::user()->role == 'admin')
                        <a href="{{ route('owners.create') }}" class="btn btn-success">Prideti</a>
                    @endif
                @endauth
            </div>
            <div class="col-sm-12">


                <table class="table">
                    @foreach($owners as $owner)
                        <tr>
                            <td>
                                <a href="{{ route('owners.show', $owner->id) }}">
                                    {{ $owner->name }}
                                </a>
                            </td>
                            @auth
                                @if(Auth::user()->role == 'admin')
                                    <td>
                                        <a href="#" class="btn btn-danger">
                                            Trinti
                                        </a>
                                    </td>
                                    @endif
                            @endauth

                        </tr>
                    @endforeach

                </table>
            </div>
        </div>
    </div>
@endsection