@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Sukurti savininka</h1>

                <form action="{{ route('owners.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <input value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror"
                               type="text" name="name"
                               placeholder="Iveskite varda"/>
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input value="{{ old('surname') }}" class="form-control @error('surname') is-invalid @enderror"
                               type="text" name="surname"
                               placeholder="Iveskite pavarde"/>
                        @error('surname')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <select class="form-control" name="car_id">
                            @foreach($cars as $car)
                                <option value="{{ $car->id }}">
                                    {{ $car->reg_number }} {{ $car->brand }}
                                </option>
                            @endforeach
                        </select>
                    </div>


                    <input type="file" name="photo" class="form-control" />

                    <input type="submit" value="Saugoti" class="btn btn-success"/>

                </form>
            </div>
        </div>
    </div>
@endsection
