@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>
                    Zmogus: {{ $owner->fullName() }}
                </h1>

                @if($owner->photo)
                    {{-- http://127.0.0.1/owner_photos/1.jpg --}}
                    {{-- http://127.0.0.1/storage/owner_photos/1.jpg --}}
                    <img src="{{ asset('storage/' . $owner->photo) }}" alt="{{ $owner->name }}" />
                @endif


                <h2> Masina:
                    <a href="{{ route('cars.show', $owner->car->id) }}">
                        {{ $owner->car->brand }}
                    </a>

                </h2>

            </div>
        </div>
    </div>
@endsection
