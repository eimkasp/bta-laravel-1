@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>
                    Automobiliai ({{ $carsCount }})
                </h1>

                <table class="table">
                    @foreach($cars as $car)
                        <tr>
                            <td>
                                <a href="{{ route('cars.show', $car->id) }}">
                                    {{ $car->reg_number }}
                                </a>
                            </td>
                            <td>{{ $car->brand }}</td>
                            <td>{{ $car->model }}</td>
                            {{-- Patikirname ar vartotojas yra prisijunges --}}
                            @auth
                                {{-- Patikriname prisijungusio varotojo role --}}
                                @if(Auth::user()->role == 'admin')
                                    <td>
                                        <a href="#" class="btn btn-danger">
                                            Trinti
                                        </a>
                                    </td>
                                @endif
                            @endauth

                        </tr>
                    @endforeach

                </table>
            </div>
        </div>
    </div>
@endsection