@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Sukurti masina</h1>

                <form action="{{ route('cars.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <input value="{{ old('reg_number') }}" class="form-control @error('reg_number') is-invalid @enderror"
                               type="text" name="reg_number"
                               placeholder="Iveskite numeri"/>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input value="{{ old('brand') }}" class="form-control @error('brand') is-invalid @enderror"
                               type="text" name="brand"
                               placeholder="Iveskite marke"/>
                        @error('brand')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input value="{{ old('model') }}" class="form-control @error('model') is-invalid @enderror"
                               type="text" name="model"
                               placeholder="Iveskite modeli"/>
                        @error('model')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>


                    <input type="file" name="photo[]" class="form-control" accept="image/*" multiple />

                    <input type="submit" value="Saugoti" class="btn btn-success"/>

                </form>
            </div>
        </div>
    </div>
@endsection
