@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>
                    Automobilis: {{ $car->reg_number }}
                </h1>

                <div class="photos row">
                    @foreach($car->photos as $photo)
                        <div class="col-sm-4">
                            <img src="{{ asset($photo->url) }}" />
                            <a href="{{ route('cars.delete.photo', $photo->id) }}">Trinti nuotrauka</a>
                        </div>
                    @endforeach
                </div>

                <h2> Savininkai</h2>
                <ul>
                    @foreach($car->owners as $owner)
                        <li>
                            {{ $owner->name }} {{ $owner->surname }}
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection
