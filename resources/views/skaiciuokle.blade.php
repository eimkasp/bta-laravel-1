<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Skaiciuokle</h1>

                <form method="POST" action="{{ route('suma') }}">
                    @csrf
                    <div>
                        <input type="text" name="x" class="form-control" />
                    </div>

                    <div>
                        <input type="text" name="y" class="form-control" />
                    </div>

                    <input type="submit" class="btn btn-success"/>
                </form>

            </div>
        </div>
    </div>

</body>
</html>