@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <h1>{{ $newsItem->title }}</h1>
                @if($newsItem->created_at)
                    Publikuotas: {{ $newsItem->created_at }}
                @endif
                <p>
                    {{ $newsItem->content }}
                </p>
            </div>
        </div>
    </div>
@endsection