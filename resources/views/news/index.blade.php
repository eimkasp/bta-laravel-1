@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Naujienos</h1>
            </div>


            @foreach($news as $item)
                <div class="col-sm-6">
                    <h3>
                        <a href="{{ route('news.show' , $item->id) }}">
                            {{ $item->title }}
                        </a>

                    </h3>
                    <p>
                        {{ $item->content }}
                    </p>
                </div>
            @endforeach
        </div>

    </div>
@endsection