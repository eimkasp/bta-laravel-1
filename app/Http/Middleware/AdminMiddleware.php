<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
// patikriname ar vartotojas prisijunges
    	if(Auth::user()) {


    		// patikriname ar vartotojo role yra admin
			if(Auth::user()->role == 'admin') {
				return $next($request);
			} else {
				return redirect()->route('cars.index');
			}
		} else {
			return redirect()->route('login');
		}






	}
}
