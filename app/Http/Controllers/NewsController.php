<?php

namespace App\Http\Controllers;

use App\Car;
use App\NewsItem;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    // naujienu saraso atvaizdavimas
	public function index() {
		// gauname visas naujienas is naujienu lenteles
		$news = NewsItem::all();

		// Perduodame $news kintamaji i savo blade faila
		return view('news.index', compact('news'));
	}

	// konkrecios naujienos atvaizdavimas
	// $id yra route parametras pasakantis pasirinktos naujienos id
	public function show($id) {
		// gaunu konkrecios naujienos objekta is duombazes
		$newsItem = NewsItem::find($id);

		return view('news.show', ["newsItem" => $newsItem]);
	}

	public function delete($id) {
		$newsItem = NewsItem::find($id);
		$newsItem->delete();
	}
}
