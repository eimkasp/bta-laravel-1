<?php

namespace App\Http\Controllers;

use App\Car;
use App\CarPhoto;
use App\Owner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CarController extends Controller {
	//

	public function index() {

		$colors = Car::getColors();

		// gauname visas masinas is duomenu bazes ir isrikiuojame pagal brand stulpeli
		$cars = Car::orderBy( 'brand', 'desc' )->get();

//		$cars = Car::all();

		// Gauname masinu kieki is duombazes su count funkcija
		$carsCount = Car::count();

		//$carsCount = Car::where('brand', '=', 'BMW')->count();

		return view( 'cars.index', compact( [ 'cars', 'carsCount' ] ) );
	}

	public function create() {
		return view( 'cars.create' );
	}

	public function store( Request $request ) {

		$car             = new Car();
		$car->brand      = $request->input( 'brand' );
		$car->reg_number = $request->input( 'reg_number' );
		$car->model      = $request->input( 'model' );
		$car->save();


		// Galima naudoti failu ikelimui: https://www.dropzonejs.com
		if ( $request->file( 'photo' ) ) {
			foreach ( $request->file( 'photo' ) as $photo ) {
				$filename = time() . $photo->getClientOriginalName(); // 55456644661.jpg
				$path     = $photo->storeAs( 'car_photos', $filename );

				$carImage         = new CarPhoto();
				$carImage->url    = 'storage/' . $path;
				$carImage->car_id = $car->id;
				$carImage->save();
			}
		}

		return redirect()->route( 'cars.index' );
	}


	/*
	 * Pasiskaitymui apie bendravima su modeliu: https://laravel.com/docs/5.8/eloquent
	 * */
	// konkrecios masinos avaizdavimas
	public function show( $id ) {
		$car       = Car::find( $id );
		$carImages = CarPhoto::where( 'car_id', '=', $id )->get();


		return view( 'cars.show', compact( [ 'car' ] ) );
	}

	public function deletePhoto( $id ) {
		$photo = CarPhoto::find( $id );

		// pasalinu storage/ is nuotraukos url

		// pries str_replace: storage/car_photos/1.jpg
		$path = str_replace( 'storage/', '', $photo->url );
		// po str_replace: car_photos/1.jpg

		// istrinu faila is failu sistemos
		Storage::delete( $path );

		// istrinu irasa duomenu bazeje
		$photo->delete();

		return redirect()->back();
	}

	public function test( $id ) {
		// $id yra konkrecios masinos id kuria reikia atvaizduoti
		$car = Car::find( $id );

		// gausime visus rezultatus tenkinancius salyga, grazinamas masyvas
		$bmwCars = Car::where( 'brand', '=', 'BMW' )
					  ->where( 'model', '=', 'Juodas' )
					  ->get();

		// gausime pirma rezultata atitinkati salyga - grazinamas Car objektas
		$bmwCars = Car::where( 'brand', '=', 'BMW' )->first();

		// surandame masinos savininkus pagal car_id stulpeli
		$owners = Owner::where( 'car_id', '=', $car->id )->get();
		// SELECT * from owner where car_id = $car->id
	}
}
