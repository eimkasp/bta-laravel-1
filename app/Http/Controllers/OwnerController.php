<?php

namespace App\Http\Controllers;

use App\Car;
use App\Http\Requests\OwnerRequest;
use App\Owner;
use Illuminate\Http\Request;

class OwnerController extends Controller {
	//

	public function index() {
		$owners = Owner::all();

		return view( 'owners.index', compact( 'owners' ) );
	}

	public function show( $id ) {
		$owner = Owner::find( $id );

		return view( 'owners.show', compact( 'owner' ) );
	}

	public function create() {

		// gauname visas masinas, kad galetume atvaizduoti
		// select'a sukurimo faile
		$cars = Car::all();

		return view( 'owners.create', compact( 'cars' ) );
	}

	public function store( OwnerRequest $request ) {
		/*
		* Visi imanomi validatoriai: https://laravel.com/docs/5.7/validation
		*/

		$owner = new Owner();

		// patikriname ar ikelta kazkokia nuotrauka
        if($request->file('photo')) {
            // pasiekiame ikelta faila
            $filename = time() . $request->file('photo')->getClientOriginalName(); // 55456644661.jpg

            // gauname failo formata
            $fileFormat = $request->file('photo')->getClientOriginalExtension(); // .jpg

            $path = $request->file('photo')->storeAs('owner_photos', $filename);
            $owner->photo = 'storage/' . $path; // storage/owner_photos/55456644661.jpg
        }



        $owner->name    = $request->input( 'name' );
		$owner->surname = $request->input( 'surname' );
		$owner->car_id  = $request->input( 'car_id' );

		$owner->save();

		return redirect()->route( 'owners.index' );
	}

	public function update(OwnerRequest $request) {

	}


}
