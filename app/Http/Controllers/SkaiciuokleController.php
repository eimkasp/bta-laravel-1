<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SkaiciuokleController extends Controller
{
    //

	function index() {
		// atvaizduojame view faila
		return view('skaiciuokle');
	}

	function suma(Request $request) {
		// echo $_POST['x'];
		// echo $request->input('x');
		$x =  $request->x;
		$y =  $request->y;

		$suma = $x + $y;

//		return view('suma', compact('suma'));
		return view('suma', [ 'suma' => $suma, 'x' => $x ]);
	}
}
