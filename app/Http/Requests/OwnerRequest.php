<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OwnerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
    	/* SVARBU: Pakeisti i true reiksme */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$rules = [
			'name' => 'required',
			'surname' => 'required|min:3|max:255',
            'photo' => 'max:1024|image',
			// cars - duombazes pavadinimas! (Ne Modelio!)
			// id - stulpelio pavadinimas
			'car_id' => 'exists:cars,id'
		];

        return $rules;
    }

    public function messages() {
    	return [
			'name.required' => 'Vardo laukelis yra privalomas.',
			'required' => ":atribute laukelis privalomas",
			'surname.min' => 'Pavarde turi buti bent 3 simboliu'
		];
	}
}
