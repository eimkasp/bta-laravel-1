<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarPhoto extends Model
{
    //

	protected $table = "car_photos";

	public function car() {
		return $this->hasOne('App\Car', 'id', 'car_id');
	}
}

