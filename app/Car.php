<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    //

	protected $table = "cars";

	// sukuriame relationship su Owner modeliu
	public function owners() {

		// nurodome modeli su kuriuo yra susijusi musu masina,
		// stulpeli owner lenteleje su kuriuo yra susijusi musu masina
		// trecias parametras, musu masinos ids
		return $this->hasMany('App\Owner', 'car_id', 'id');
	}

	public function photos() {
		// nurodome modeli su kuriuo yra susijusi musu masina,
		// stulpeli owner lenteleje su kuriuo yra susijusi musu masina
		// trecias parametras, musu masinos ids
		return $this->hasMany('App\CarPhoto', 'car_id', 'id');
	}

	public static function getColors() {
		$array = ['red', 'black', 'yellow'];

		return $array;
	}

}
