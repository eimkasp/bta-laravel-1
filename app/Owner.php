<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    //
	protected $table = "owners";


	/* Owner relationship su Car modeliu */
	public function car() {
		return $this->hasOne('App\Car', 'id', 'car_id');
	}

	public function fullName() {
		return $this->name . " " . $this->surname;
	}


}
