<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeyForOwnersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table( 'owners', function ( Blueprint $table ) {

			// indexo sukurimas per migracija
			$table->index( 'car_id' );

			// sukuriu car_id stulpeliui foreign key
			$table->foreign( 'car_id' )
				  ->references( 'id' )
				  ->on( 'cars');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'foreign_key_for_owners' );
	}
}
