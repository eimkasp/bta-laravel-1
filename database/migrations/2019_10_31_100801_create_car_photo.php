<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarPhoto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_photos', function (Blueprint $table) {
            //
            $table->bigIncrements('id');
			$table->string('url');
			$table->bigInteger('car_id')->unsigned();
			$table->timestamps();

			// sukurtin indexa car_id stulpeliui
            // aprasyti foreign keys
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('car_photos', function (Blueprint $table) {
            //
        });
    }
}
