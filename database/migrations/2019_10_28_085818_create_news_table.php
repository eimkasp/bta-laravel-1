<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

    	// Lenteles sukurimas
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('content');
            $table->string('main_image');
            $table->bigInteger('author_id')->unsigned();
            $table->timestamps(); // sukuria du stulpelius created_at ir updated_at
        });

        // Jei norim kad susikurtu musu lentele, tai reikia paleisti komanda:
		// php artisan migrate
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
