<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToCarPhotosTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table( 'car_photos', function ( Blueprint $table ) {
			// sukuriame indexa car_id stulpeliui
			$table->index( 'car_id' );

			// sukuriame foreign key su cars lentele
			$table->foreign( 'car_id' )
				  ->references( 'id' )
				  ->on( 'cars' )
				  ->onDelete( 'cascade' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table( 'car_photos', function ( Blueprint $table ) {
			//
		} );
	}
}
