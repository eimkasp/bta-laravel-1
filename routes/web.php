<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get( '/', function () {
	return view( 'welcome' );
} );


Route::get( '/skaiciuokle', 'SkaiciuokleController@index' )->name( 'skaiciuokle' )
	 ->middleware( 'auth' );

Route::post( '/suma', 'SkaiciuokleController@suma' )->name( 'suma' );


Route::get( '/kmi', 'KMIController@index' );
Route::post( '/kmi', 'KMIController@rezultatas' );


Auth::routes();
Route::get( '/home', 'HomeController@index' )->name( 'home' );

// Visu automobiliu atvaizdavimas
Route::get( '/cars', 'CarController@index' )->name( 'cars.index' );

Route::get( '/cars/create', 'CarController@create' )->name( 'cars.create' );
Route::post( '/cars/store', 'CarController@store' )->name( 'cars.store' );
Route::get( '/cars/photo/{id}/delete', 'CarController@deletePhoto' )->name( 'cars.delete.photo' );

Route::get( '/cars/{id}', 'CarController@show' )->name( 'cars.show' );


// Visu savininku atvaizdavimas
Route::get( '/owners', 'OwnerController@index' )
	 ->name( 'owners.index' )
	 ->middleware( ['auth'] );
Route::get( '/owners/create', 'OwnerController@create' )
	 ->name( 'owners.create' )
	 ->middleware( 'admin' );


Route::post( '/owners/store', 'OwnerController@store' )
	 ->name( 'owners.store' )
	 ->middleware( 'admin' );

Route::get( '/owners/{id}', 'OwnerController@show' )->name( 'owners.show' );

// Visu naujienu atvaizdavimas
Route::get( '/news', 'NewsController@index' )->name( 'news.index' );
// Konkrecios naujienos atvaizdavimas
Route::get( '/news/{id}', 'NewsController@show' )->name( 'news.show' );



Route::resource('test', 'TestController');